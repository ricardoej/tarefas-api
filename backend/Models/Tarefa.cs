﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TarefaApi.Model
{
  public class Tarefa
  {
    // Por convenção o EF já entende que uma propriedade Id é uma chave primária.
    // Se eu colocasse a propriedade com outro nome qualquer e quisesse colocar
    // como chave primária, seria necessário colocar o atributo:
    // [Key]
    // Mais detalhes em https://www.entityframeworktutorial.net/code-first/column-dataannotations-attribute-in-code-first.aspx
    public int Id { get; set; }

    // Por convenção, o EF coloca a coluna no banco com o mesmo nome da propriedade.
    // Se eu quisesse alterar, teria que colocar o atributo
    // [Column("OutroNomeParaColuna")]
    // Também é possível alterar o tipo e outras informações
    // Mais detalhes em https://www.entityframeworktutorial.net/code-first/column-dataannotations-attribute-in-code-first.aspx
    public string Descricao { get; set; }

    public int Prioridade { get; set; }

    // Relacionamento one-to-one. Este é feito por convenção. Apenas colocando a propriedade aqui
    // e na classe Execucao colocando uma propriedade Tarefa e um TarefaId, o EF entende que isso
    // é um relacionamento one-to-one e cria a relação no banco de dados
    // Mais detalhes em https://www.learnentityframeworkcore.com/conventions/one-to-one-relationship
    public Execucao Execucao { get; set; }

    // Relacionamento one-to-many Este é feito por convenção. Apenas colocando a propriedade aqui
    // (como uma coleção), o EF entende que isso é um relacionamento one-to-many e cria a
    // relação no banco de dados
    // Mais detalhes em https://www.learnentityframeworkcore.com/conventions/one-to-many-relationship
    public ICollection<Arquivo> Arquivos { get; set; }

    // Relacionamento many-to-many. Este é um pouco mais complicado. É necessário criar uma classe
    // de relação (TarefaResponsavel) para que o EF entenda que será necessário criar uma tabela
    // de relação muitos-para-muitos e fazer o mapeamento na classe TarefaContext (método OnModelCreating)
    // Mais detalhes em https://www.learnentityframeworkcore.com/configuration/many-to-many-relationship-configuration
    [JsonIgnore]
    public ICollection<TarefaResponsavel> TarefaResponsaveis { get; set; }

    public IEnumerable<Responsavel> Responsaveis { get { return TarefaResponsaveis?.Select(x => x.Responsavel); } }
  }
}
