﻿using Microsoft.EntityFrameworkCore;

namespace TarefaApi.Model
{
  public class ApiDbContext : DbContext
  {
    public ApiDbContext(DbContextOptions<ApiDbContext> options)
        : base(options)
    { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<TarefaResponsavel>()
          .HasKey(tr => new { tr.TarefaId, tr.ResponsavelId });
      modelBuilder.Entity<TarefaResponsavel>()
          .HasOne(tr => tr.Tarefa)
          .WithMany(t => t.TarefaResponsaveis)
          .HasForeignKey(tr => tr.TarefaId);
      modelBuilder.Entity<TarefaResponsavel>()
          .HasOne(tr => tr.Responsavel)
          .WithMany(r => r.TarefaResponsaveis)
          .HasForeignKey(tr => tr.ResponsavelId);
    }

    public DbSet<Tarefa> Tarefas { get; set; }
    public DbSet<Execucao> Execucoes { get; set; }
    public DbSet<Arquivo> Arquivos { get; set; }
  }
}